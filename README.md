#include<stdlib.h>
#include<stdio.h>
#include <conio.h>
#include <graphics.h>

#pragma comment(lib,"Winmm.lib")

#define High 700 // 游戏画面尺寸
#define Width 500

// 全局变量
int i=1;
int score;

IMAGE   img_bk;//背景图片

IMAGE  img_planeNormal1,img_planeNormal2;//正常飞机图片
int  position_x,position_y;//飞机坐标

IMAGE img_bullet1,img_bullet2;//子弹图片
int bullet_x,bullet_y;//子弹坐标

IMAGE  img_enemyplane1,img_enemyplane2;//正常飞机图片
float  enemy_x,enemy_y;//敌机坐标

IMAGE img_planeexplode1,img_planeexplode2;//爆炸飞机照片
int explode_x,explode_y;//炸机坐标


void startup()  // 数据初始化
{
    score=0;

    mciSendString("open E:\\game_music.mp3 alias bkmusic",NULL,0,NULL);//背景音乐
    mciSendString("play bkmusic repeat",NULL,0,NULL);//循环播放
    

    initgraph(Width, High);
    loadimage(&img_bk,"E:\\素材\\飞机大战图片音乐素材\\background.jpg");
    loadimage(&img_planeNormal1,"E:\\素材\\飞机大战图片音乐素材\\planeNormal_1.jpg");
    loadimage(&img_planeNormal2,"E:\\素材\\飞机大战图片音乐素材\\planeNormal_2.jpg");

    position_x=Width/2;
    position_y=High*0.7;

    loadimage(&img_bullet1,"E:\\素材\\飞机大战图片音乐素材\\bullet1.jpg");
    loadimage(&img_bullet2,"E:\\素材\\飞机大战图片音乐素材\\bullet2.jpg");

    bullet_x=position_x;
    bullet_y=-100;

    loadimage(&img_enemyplane1,"E:\\素材\\飞机大战图片音乐素材\\enemyPlane1.jpg");
    loadimage(&img_enemyplane2,"E:\\素材\\飞机大战图片音乐素材\\enemyPlane2.jpg");
    enemy_x=Width*0.5;
    enemy_y=0;

    loadimage(&img_planeexplode1,"E:\\素材\\飞机大战图片音乐素材\\planeExplode_1.jpg");
    loadimage(&img_planeexplode2,"E:\\素材\\飞机大战图片音乐素材\\planeExplode_2.jpg");

    BeginBatchDraw();


}

void clean()  // 显示画面
{
    
}    

void show()  // 显示画面
{
    putimage(0,0,&img_bk);
    putimage(position_x-50,position_y-50,&img_planeNormal1,NOTSRCERASE);//显示正常飞机
    putimage(position_x-50,position_y-50,&img_planeNormal2,SRCINVERT);

    putimage(bullet_x,bullet_y,&img_bullet1,NOTSRCERASE);//显示子弹
    putimage(bullet_x,bullet_y,&img_bullet2,SRCINVERT);

    putimage(enemy_x,enemy_y,&img_enemyplane1,NOTSRCERASE);//显示敌机
    putimage(enemy_x,enemy_y,&img_enemyplane2,SRCINVERT);
    
    outtextxy(Width*0.48,High*0.95,"得分：");
    char s[5];
    sprintf(s,"%d",score);
    outtextxy(Width*0.55,High*0.95,s);
       


        // 延时
    FlushBatchDraw();
    Sleep(5);    
}    

void updateWithoutInput()  // 与用户输入无关的更新
{

    if(bullet_y>-40)
    bullet_y-=2;//子弹向上移动

    if (enemy_y<High)
        enemy_y+=0.5;
    else if(enemy_y-High>0&&enemy_y-High<300)
    {
        enemy_x=rand()%(Width-100);
        enemy_y=0;
    }
    
    if (abs(enemy_x-bullet_x)+abs(enemy_y-bullet_y)<50)//子弹击中敌机
    {
        enemy_x=rand()%(Width-100);
        enemy_y=0;
        bullet_y=-100;
        score++;
        mciSendString("close gotenemy",NULL,0,NULL);//关闭先前一次的音乐
        mciSendString("open E:\\gotEnemy.mp3 alias  gotenemy",NULL,0,NULL);
        mciSendString("play  gotenemy",NULL,0,NULL);

    }
    if (abs(enemy_x-position_x)+abs(enemy_y-position_y)<150)//敌机击中我机
        i=0;
     if(score==0)
     {

     }
    else if (score%5==0&&score%2!=0)
    {
        mciSendString("close 5",NULL,0,NULL);//关闭先前一次的音乐
        mciSendString("open E:\\5.mp3 alias  5",NULL,0,NULL);
        mciSendString("play  5",NULL,0,NULL);
    }
    else if (score%5==0&&score%2==0)
    {
        mciSendString("close 10",NULL,0,NULL);//关闭先前一次的音乐
        mciSendString("open E:\\10.mp3 alias  10",NULL,0,NULL);
        mciSendString("play  10",NULL,0,NULL);
    }
    
}

void updateWithInput()  // 与用户输入有关的更新
{
    MOUSEMSG m;//记录鼠标消息
    while (MouseHit())
    {
        m=GetMouseMsg();
        if (m.uMsg==WM_MOUSEMOVE)//鼠标移动时，飞机坐标等于鼠标位置
        {
            position_x=m.x;
            position_y=m.y;
        }
        else if (m.uMsg==WM_LBUTTONDOWN)//按下鼠标左键时发射子弹
        {
            mciSendString("close f_gun",NULL,0,NULL);//关闭先前一次的音乐
            mciSendString("open E:\\f_gun.mp3 alias  f_gun",NULL,0,NULL);
            mciSendString("play  f_gun",NULL,0,NULL);

            bullet_x=position_x;
             bullet_y=position_y-85;
        }
    }







}

void gameover()
{
    putimage(0,0,&img_bk);
    putimage(position_x-50,position_y-50,&img_planeNormal1,NOTSRCERASE);//显示正常飞机
    putimage(position_x-50,position_y-50,&img_planeNormal2,SRCINVERT);

    putimage(position_x-50,position_y-50,&img_planeexplode1,NOTSRCERASE);//显示炸毁飞机
    putimage(position_x-50,position_y-50,&img_planeexplode2,SRCINVERT);

    mciSendString("close explode",NULL,0,NULL);//关闭先前一次的音乐
    mciSendString("open E:\\explode.mp3 alias  explode",NULL,0,NULL);
    mciSendString("play  explode",NULL,0,NULL);


    EndBatchDraw();
    getch();
    closegraph();
}

int main()
{
    startup();  // 数据初始化    
    while (i)  //  游戏循环执行
    {
        clean();  // 把之前绘制的内容清除
        updateWithoutInput();  // 与用户输入无关的更新
        updateWithInput();     // 与用户输入有关的更新
        show();  // 显示新画面
    }
    gameover();     // 游戏结束、后续处理
    return 0;
}

